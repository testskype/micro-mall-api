module gitee.com/cristiane/micro-mall-api

go 1.13

require (
	gitee.com/cristiane/go-common v1.0.1
	gitee.com/kelvins-io/common v1.0.2
	gitee.com/kelvins-io/kelvins v1.2.2
	github.com/RichardKnop/machinery v1.9.1
	github.com/astaxie/beego v1.12.2
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ini/ini v1.60.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/grpc-ecosystem/grpc-gateway v1.14.7
	github.com/jinzhu/gorm v1.9.16
	github.com/prometheus/client_golang v1.7.1
	github.com/satori/go.uuid v1.2.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/sony/sonyflake v1.0.0
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/ini.v1 v1.60.2 // indirect
	xorm.io/xorm v1.0.4
)
